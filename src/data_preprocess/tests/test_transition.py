import unittest
from data_preprocess import transition
from data_preprocess.transition import Transition
from datetime import datetime
import pandas as pd
import numpy as np

TEST_DATA_PATH = 'src/data_preprocess/tests/transition_test_data/'


class TransitionTestCase(unittest.TestCase):

    def test_constants_type(self):
        time_periods_type = type(transition.TIME_PERIODS_IN_DAY)
        self.assertTrue(time_periods_type == int)

        csv_path_type = type(transition.CSV_PATH)
        self.assertTrue(csv_path_type == str)

        zip_codes_path_type = type(transition.ZIP_CODES_PATH)
        self.assertTrue(zip_codes_path_type == str)

        pickle_path_type = type(transition.PICKLE_PATH)
        self.assertTrue(pickle_path_type == str)

    def test_map_to_period(self):
        date = datetime(1994, 4, 3, 2, 44, 1)
        time_periods = 4
        correct_index = 0
        test_index = Transition.map_to_period(date, time_periods)
        self.assertTrue(correct_index == test_index)

        date = datetime(1998, 2, 5, 23, 44, 1)
        time_periods = 4
        correct_index = 3
        test_index = Transition.map_to_period(date, time_periods)
        self.assertTrue(correct_index == test_index)

    def test_calculate_matrix(self):
        zip_code_dict = {0: 0, 1: 1}

        df = pd.DataFrame({'pickup_zips': [0, 1], 'dropoff_zips': [0, 1]})
        matrix = Transition.calculate_matrix(df, zip_code_dict)

        correct_matrix = [[1, 0], [0, 1]]
        self.assertTrue(np.array_equal(matrix, correct_matrix))

        df = pd.DataFrame(
            {'pickup_zips': [0, 0, 1], 'dropoff_zips': [1, 0, 1]})
        matrix = Transition.calculate_matrix(df, zip_code_dict)

        correct_matrix = [[0.5, 0.5], [0, 1]]
        self.assertTrue(np.array_equal(matrix, correct_matrix))

    def test_calculate_matrices(self):
        obj = Transition(load_data=False,
                         zip_codes_path=TEST_DATA_PATH + 'DummyZipCodes.json',
                         csv_path=TEST_DATA_PATH + 'transition_dummy_data.csv',
                         pickle_path=(TEST_DATA_PATH +
                                      'dummy_matrices.pickle'),
                         time_periods=2)
        calculated_matrices = obj.matrices
        correct_matrices = [[1., 0.],
                            [0., 1.]], [[0., 1.],
                                        [0., 0.]]
        self.assertTrue(np.array_equal(calculated_matrices, correct_matrices))

    def test_pickle_save_and_load(self):
        obj_1 = Transition(
            load_data=False,
            zip_codes_path=TEST_DATA_PATH + 'DummyZipCodes.json',
            csv_path=TEST_DATA_PATH + 'transition_dummy_data.csv',
            pickle_path=TEST_DATA_PATH + 'dummy_matrices.pickle',
            time_periods=2)

        obj_2 = Transition(
            load_data=True,
            zip_codes_path=TEST_DATA_PATH + 'DummyZipCodes.json',
            csv_path=TEST_DATA_PATH + 'transition_dummy_data.csv',
            pickle_path=TEST_DATA_PATH + 'dummy_matrices.pickle',
            time_periods=2)

        saved_matrices = obj_1.matrices
        loaded_matrices = obj_2.matrices
        self.assertTrue(np.array_equal(saved_matrices, loaded_matrices))

    def test_simulate_new_dropoff_zone(self):
        obj = Transition(
            load_data=False,
            zip_codes_path=TEST_DATA_PATH + 'DummyZipCodes.json',
            csv_path=TEST_DATA_PATH + 'transition_dummy_data.csv',
            pickle_path=TEST_DATA_PATH + 'dummy_matrices.pickle',
            time_periods=2)

        test_date = datetime(1994, 4, 3, 2, 44, 1)
        new_dropoff = obj.simulate_new_dropoff_zone(10026, test_date, 0.01)
        self.assertTrue(new_dropoff == 10026)

    def test_no_observations(self):
        obj = Transition(
            load_data=False,
            zip_codes_path=TEST_DATA_PATH + 'DummyZipCodes.json',
            csv_path=TEST_DATA_PATH + 'transition_dummy_data.csv',
            pickle_path=TEST_DATA_PATH + 'dummy_matrices.pickle',
            time_periods=2)
        test_date = datetime(1994, 4, 3, 13, 44, 1)
        should_be_true = obj.no_observations(10027, test_date)
        self.assertTrue(should_be_true)

        test_date = datetime(1994, 4, 3, 10, 44, 1)
        should_be_false = obj.no_observations(10027, test_date)
        self.assertFalse(should_be_false)


if __name__ == '__main__':
    unittest.main()
